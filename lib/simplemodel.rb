require "active_support/core_ext/class/attribute_accessors"
require "active_support/core_ext/class/attribute"
require "active_support/core_ext/hash/indifferent_access"
require "active_model"

$:.unshift(File.dirname(__FILE__))
require "simplemodel/ext/typed_array"
module SimpleModel
  autoload :Association, "simplemodel/association"
  autoload :Base,        "simplemodel/base"
end

SimpleModel::Base.include_root_in_json = false

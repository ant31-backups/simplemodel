class TypedArray < Array

  def initialize(klass, *args)
    @klass = klass
    super *args
  end

  def <<(elt)
    check_klass(elt)
    super(elt)
  end

  def push(*args)
    check_klass(*args)
    super *args
  end

  private
  def check_klass(*elts)
    elts.each do |e|
      if not e.instance_of?(@klass)
        raise TypeError, "can't convert #{e.class.name} into #{@klass.name}: #{e}"
      end
    end
    return true
  end
end

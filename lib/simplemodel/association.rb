require "active_support/core_ext/string/inflections.rb"

module SimpleModel
  module Association
    module ClassMethods

      def associations
        @associations ||= {}
      end

      def one(to_model, options = {})
        to_model    = to_model.to_s
        class_name  = options[:class_name]  || to_model.classify
        self.associations[to_model.to_s.to_sym] = {:class_name => class_name, :name => to_model}
        class_eval(<<-EOS, __FILE__, __LINE__ + 1)
          attr_reader :#{to_model}

          def #{to_model}
            @#{to_model}
          end#

          def clear_#{to_model}
            @#{to_model} = nil
          end

          def #{to_model}=(elt)
            if elt.kind_of?(Hash)
              instance = #{class_name}.new(elt)
            elsif elt.instance_of?(#{class_name}) || elt == nil
              instance = elt
            else
              raise TypeError, "Expected Hash of attributes or #{class_name.name}"
            end
            @#{to_model} = instance
          end

        EOS
      end

      def many(to_model, options = {})
        to_model    = to_model.to_s
        class_name  = options[:class_name]  || to_model.classify
        name = to_model
        self.associations[to_model.to_s.to_sym] = {:class_name => class_name, :name => to_model}

        class_eval(<<-EOS, __FILE__, __LINE__ + 1)
          attr_reader :#{to_model}

          def #{to_model}
            @#{to_model} ||= TypedArray.new(#{class_name})
          end#

          def clear_#{to_model}
            @#{to_model} = nil
          end

          def #{to_model}=(array)
            @#{to_model} = TypedArray.new(#{class_name})
            array.each do |elt|
              if elt.kind_of?(Hash)
                instance = #{class_name}.new(elt)
              else
                instance = elt
              end
              self.#{to_model} << instance
            end
            return self.#{to_model}
          end

          def add_#{to_model}(*args)
            @#{to_model} = TypedArray.new(#{class_name})
            args.each do |attr|
                instance = #{class_name}.new(attr)
                self.#{to_model} << instance
            end
          end
        EOS
      end
    end

    module Model
      def self.included(base)
        base.extend(ClassMethods)
      end
    end
  end
end

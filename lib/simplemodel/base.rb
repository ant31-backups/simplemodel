module SimpleModel
  class Base
    class_attribute :known_attributes, :raw_attributes
    self.known_attributes = []
    self.raw_attributes = {}
    class << self
      def define_attributes(*attr)
        self.raw_attributes = {}
        attr.each{|a| self.raw_attributes[a] = nil}
        attributes *attr
      end

      def attributes(*attributes)
        self.known_attributes |= attributes.map(&:to_s)
      end
    end

    attr_accessor :attributes, :unknown_attributes

    def refresh()
      self.attributes.each do |name, attr|
        res = self.send "#{name}=".to_sym, attr
        if not self.raw_attributes.has_key?(name.to_s.to_sym)
          self.attributes.delete(name)
        end
      end
      return self
    end

    def known_attributes
      self.class.known_attributes | self.attributes.keys.map(&:to_s)
    end

    def raw_attributes
      self.class.raw_attributes
    end

    def attributes
      @attributes
    end

    def attributes=(attr)
      @attributes = {}.with_indifferent_access
      @attributes.merge!(self.raw_attributes)
      @attributes.merge!(attr)
      self.refresh()
      return @attributes
    end

    def initialize(opts = {})
      @unknown_attributes = {}
      self.attributes = opts
    end

    def has_attribute?(name)
      self.attributes.has_key?(name)
    end

    alias_method :respond_to_without_attributes?, :respond_to?

    def respond_to?(method, include_priv = false)
      method_name = method.to_s
      if self.attributes.nil?
        super
      elsif known_attributes.include?(method_name)
        true
      elsif method_name =~ /(?:=|\?)$/ && self.attributes.include?($`)
        true
      else
        super
      end
    end


      protected
      def read_attribute(name)
        @attributes[name]
      end

      def write_attribute(name, value)
        @attributes[name] = value
        self.refresh()
      end

      private

      def method_missing(method_symbol, *arguments) #:nodoc:
        method_name = method_symbol.to_s
        if method_name =~ /(=|\?)$/
          case $1
          when "="
            if not self.raw_attributes.has_key?($`.to_s.to_sym)
              self.attributes.delete($`)
              self.unknown_attributes[$`] = arguments.first
            else
              @attributes[$`] = arguments.first
            end
          when "?"
              @attributes.has_key?($`)
          end
        else
          return attributes[method_name] if attributes.has_key?(method_name)
          super
        end
      end
    end


  class Base
    extend  ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Serializers::JSON
    include ActiveModel::Serializers::Xml
    include Association::Model
  end

end
